#! /usr/bin/env python
# -*- coding: utf-8 -*-

import random
import requests


from chatbot.SamaraChatBot import SamaraChatBot

def main():
    samaraChatBot = SamaraChatBot()
    samaraChatBot.run()

if __name__ == "__main__":
    main()