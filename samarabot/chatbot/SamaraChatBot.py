# Schedule Library imported
import schedule
import time
import datetime

from lib.logger.SamaraChatBotLogger import SamaraChatBotLogger
from command.GoodMorningCommand import GoodMorningCommand
from command.HappyBirthdayCommand import HappyBirthdayCommand

class SamaraChatBot(object):
    def __init__(this):
        this.initScheduleCommands()

    def initScheduleCommands(this):
        schedule.every().day.at(GoodMorningCommand.getScheduledTime()).do(this.goodMorning)
        schedule.every().day.at(HappyBirthdayCommand.getScheduledTime()).do(this.happyBirthday)

    def goodMorning(this):
        goodMorningCommand = GoodMorningCommand()
        goodMorningCommand.execute()

    def happyBirthday(this):
        happyBirthdayCommand = HappyBirthdayCommand()
        happyBirthdayCommand.execute()

    def run(this):
        while True:
            try:
                schedule.run_pending()
                print(str(datetime.datetime.now()))
                time.sleep(1)
            except Exception as e:
                print('ERROR: ' + str(e))
                #! SamaraChatBotLogger.writeToLog('ERROR: ' + str(e))