#! /usr/bin/env python
# -*- coding: utf-8 -*-

import requests,lxml.html,re,json

class LadaSamara(object):

    def __init__(this):
        this.login = '89278875031'
        this.password = 'Regsinaz4921'
        this.hashes = {}
        this._auth()

    def executeMethod(this,method,v=5.87,**params):
        if method not in this.hashes:
            this._getHash(method)
        data = {'act': 'a_run_method','al': 1,
                'hash': this.hashes[method],
                'method': method,
                'param_v':v}
        for i in params:
            data["param_"+i] = params[i]
        answer = this.session.post('https://vk.com/dev',data=data)
        return json.loads(re.findall("<!>(\{.+)",answer.text)[-1])['response']

    def _auth(this):
        response = this._requestAuthInVk()
        if "onLoginDone" not in response.text:
            raise InvalidPassword("Invalid Password!")

    def _requestAuthInVk(this):
        this.session = requests.session()
        data = this.session.get('https://vk.com/', headers=this._getHeaders())
        form = this._prepareLogInForm(data)
        return this.session.post(form.action, data=form.form_values())

    def _prepareLogInForm(this, data):
        page = lxml.html.fromstring(data.content)
        form = page.forms[0]
        form.fields['email'] = this.login
        form.fields['pass'] = this.password
        return form

    def _getHeaders(this):
        return {
           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Language':'ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3',
           'Accept-Encoding':'gzip, deflate',
           'Connection':'keep-alive',
           'DNT':'1'
        }

    def _getHash(this,method):
        html = this.session.get('https://vk.com/dev/'+method)
        hash_0 = re.findall('onclick="Dev.methodRun\(\'(.+?)\', this\);',html.text)
        if len(hash_0)==0:
            raise NotValidMethod("method is not valid")
        this.hashes[method] = hash_0[0]
