import datetime

class SamaraChatBotLogger(object):
    @staticmethod
    def writeToLog(message):
        samaraLogPath = "/var/wwww/samarabot/chatbot/log/samarachatbot.log"
        logMessageFormat = '[{0}] {1} \n'
        logFile = open(samaraLogPath, 'a')
        logFile.write(logMessageFormat.format(str(datetime.datetime.now()), message))
        logFile.close()