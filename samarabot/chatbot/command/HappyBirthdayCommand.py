#! /usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import re

# Устранение проблем с кодировкой UTF-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from ICommand import ICommand

from ..exception.InvalidPassword import InvalidPassword
from ..exception.NotValidMethod import NotValidMethod
from ..lib.api_user.LadaSamara import LadaSamara

class HappyBirthdayCommand(ICommand):
    def __init__(this):
        this.ladaSamara = LadaSamara()

    def execute(this):
        clubUsers = this._getClubUsers()
        fullUsersInfo = this._getFullClubUsersProfileInfo(clubUsers)
        print(fullUsersInfo)
        usersWhichNeedToCongratulate = this._getUsersWhichNeedToCongratulate(fullUsersInfo)
        print(usersWhichNeedToCongratulate)
        if len(usersWhichNeedToCongratulate) != 0:
            response = this.ladaSamara.executeMethod("messages.send", chat_id=1, message=this._prepareMessage(usersWhichNeedToCongratulate))
        print("Execute Happy Birthday Command")

    @staticmethod
    def getScheduledTime():
        return "07:00"

    def _getClubUsers(this):
        response = this.ladaSamara.executeMethod("messages.getChat", chat_id=1)
        return response['users']

    def _getFullClubUsersProfileInfo(this, users):
        userIds = ','.join(map(str, users))
        fields = "first_name, last_name, bdate, id"
        response = this.ladaSamara.executeMethod("users.get", user_ids=userIds, fields=fields)
        return response

    def _getUsersWhichNeedToCongratulate(this, fullUsersInfo):
        result = []
        today = datetime.date.today()
        print(today.strftime("%e.%m").replace(".0", "."))
        todayWithoutYear = today.strftime("%e.%m").replace(".0", ".")
        for fullUserInfo in fullUsersInfo:
            if 'bdate' not in fullUserInfo:
                continue
            match = re.search(r'\d{1,2}[.]\d{1,2}', fullUserInfo['bdate'])
            birthday = match.group(0)
            if (birthday == todayWithoutYear):
                result.append(fullUserInfo)
        return result

    def _prepareMessage(this, usersWhichNeedToCongratulate):
        usersCount = len(usersWhichNeedToCongratulate)
        message = "Ребята, не забудьте поздравить! Сегодня день рождения " + ("отмечает:" if (usersCount == 1) else "отмечают:")
        for user in usersWhichNeedToCongratulate:
            message += " [id" + str(user['id']) + "|" + user['first_name'] + " " + user['last_name'] + "]"
        return message