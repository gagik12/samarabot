#! /usr/bin/env python
# -*- coding: utf-8 -*-

from ICommand import ICommand

from ..exception.InvalidPassword import InvalidPassword
from ..exception.NotValidMethod import NotValidMethod
from ..lib.api_user.LadaSamara import LadaSamara

class GoodMorningCommand(ICommand):

    def execute(this):
        ladaSamara = LadaSamara()
        ladaSamara.executeMethod("messages.send", chat_id=1, message=this._getMessage())
        print("Execute Good Morning Command")

    @staticmethod
    def getScheduledTime():
        return "06:00"

    def _getMessage(this):
        return "Доброе утро!"
