from abc import ABCMeta, abstractmethod

class ICommand:
    __metaclass__ = ABCMeta

    @abstractmethod
    def execute(this):
        raise NotImplementedError

    @abstractmethod
    def getScheduledTime(this):
        raise NotImplementedError